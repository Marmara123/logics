
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>tasks</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">    
</head>
<body>
    <h1><span class="label label-default ml-3">PHP</span></h1>
    <a href="http://localhost/Logics/josephusSurvive.php"><button type="button" class="btn btn-primary">JosephusSurvivor</button></a>
    <a href="http://localhost/Logics/NestingStructureComparison.php"><button type="button" class="btn btn-success">NestingStructureComparison</button></a>
    <a href="http://localhost/Logics/peckpeaks.php"><button type="button" class="btn btn-info">PeakPeaks</button></a>
    <a href="http://localhost/Logics/SudokuLogic.php"><button type="button" class="btn btn-warning">SudokuLogics</button></a>
    <a href="http://localhost/Logics/TwiceLinearSolution.php"><button type="button" class="btn btn-danger">TwiceLinear</button></a>
    <a href="http://localhost/Logics/FindTheUniqueString.php"><button type="button" class="btn btn-primary">FindTheUniqueString</button></a>
    <a href="http://localhost/Logics/MeanSquareError.php"><button type="button" class="btn btn-warning">MeanSquareError</button></a>
    <a href="http://localhost/Logics/SumByFactors.php"><button type="button" class="btn btn-success">SumByFactors</button></a>
    
</body>
</html>

