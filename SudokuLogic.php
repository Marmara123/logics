<html>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>

<?php 

class sudokuSolver{
   static protected $sudoku=[
        [5, 3, 4, 6, 7, 8, 9, 1, 2],
        [6, 7, 2, 1, 9, 5, 3, 4, 8],
        [1, 9, 8, 3, 4, 2, 5, 6, 7],
        [8, 5, 9, 7, 6, 1, 4, 2, 3],
        [4, 2, 6, 8, 5, 3, 7, 9, 1],
        [7, 1, 3, 9, 2, 4, 8, 5, 6],
        [9, 6, 1, 5, 3, 7, 2, 8, 4],
        [2, 8, 7, 4, 1, 9, 6, 3, 5],
        [3, 4, 5, 2, 8, 6, 1, 7, 9]
    ];
    public static function mainCheck(){
        $sudoku= self::$sudoku;
        $counter=3;
        $counter1=0;
        $MatrixReWriteFinal=[];
        for($ik=0; $ik<9; $ik++){
            $colrewrite=[];
            $MatrixReWrite=[];
            for($mk=0; $mk<9;$mk++){
                array_push($colrewrite, $sudoku[$mk][$ik]);
                if(in_array( $sudoku[$mk],$sudoku[$ik])){
                    return false;
                }
            }
            for($mk=0; $mk<9;$mk++){
                if(in_array($colrewrite[$ik][$mk],$colrewrite)){
                   return false;
                }
            }   
            for($i=0; $i<9; $i++){
                if($ik<$counter){
                    array_push($MatrixReWrite, $sudoku[$ik][$i]);
                }
            }   
            array_push($MatrixReWriteFinal, $MatrixReWrite); 
            $counter+=3;


        }
        for($i=0; $i<9; $i++){
            for($k=0; $k<9;$k++){
                if(in_array($MatrixReWriteFinal[$k], $MatrixReWriteFinal[$i])){
                    return false;
                }
            }            
        }
        // echo '<script language="javascript">';
        // echo 'alert("message successfully sent")';  
        // echo '</script>';
        return true;
    }
    public static function sweet(){
        if(sudokuSolver::mainCheck()){
            echo '<script language="javascript">';
            echo 'sweetAlert("Sudoku", "Solved Succesfully", "success")';  
            echo '</script>';
        }else{
            echo '<script language="javascript">';
            echo 'sweetAlert("Sudoku", "Sorry Cannt Solve", "error")';  
            echo '</script>';
        }
    }
}




sudokuSolver::sweet();