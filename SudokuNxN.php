<html>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>

<?php 

class sudokuSolver{
   static protected $sudoku=[
        [1,2,3,4],
        [3,4,2,1],
        [2,1,4,3],
        [4,3,1,2]
    ];
    public static function mainCheck(){
        $sudoku= self::$sudoku;
        $check=sudokuSolver::checkbeforesolving();
        $size= sizeof($sudoku);
        $pow= sqrt($size);
        if(!$check){
            return false;
        }
        if(!self::check()){
            return false;
        }
        return true;
    }



    public static function checkbeforesolving(){
        $sudoku= self::$sudoku;
        $size= sizeof($sudoku);
        if($size!=sizeof($sudoku[0])){
            return false;
        }
        for($i=0; $i<$size; $i++){
            if($size!=sizeof($sudoku[$i])){
                return false;
            }
        }
        $pow= sqrt($size);
        if((int)$pow==$pow){
            return true;
        }else{
            return false;
        }
    }

    public static function check(){
        $sudoku= self::$sudoku;
        $size= sizeof($sudoku);
        $counter=sqrt($size);
        $counter1=0;
        $MatrixReWriteFinal=[];
        for($ik=0; $ik<$size; $ik++){
            $colrewrite=[];
            $MatrixReWrite=[];
            for($mk=0; $mk<$size;$mk++){
                array_push($colrewrite, $sudoku[$mk][$ik]);
                if(in_array( $sudoku[$mk],$sudoku[$ik])){
                    return false;
                }
            }
            for($mk=0; $mk<$size;$mk++){
                if(in_array($colrewrite[$ik][$mk],$colrewrite)){
                   return false;
                }
            }   
            for($i=0; $i<$size; $i++){
                if($ik<$counter){
                    array_push($MatrixReWrite, $sudoku[$ik][$i]);
                }
            }   
            array_push($MatrixReWriteFinal, $MatrixReWrite); 
            $counter+=sqrt($size);;

        }
        for($i=0; $i<$size; $i++){
            for($k=0; $k<$size;$k++){
                if(in_array($MatrixReWriteFinal[$k], $MatrixReWriteFinal[$i])){
                    return false;
                }
            }            
        }
        return true;
    } 




    public static function sweet(){
        if(sudokuSolver::mainCheck()){
            echo '<script language="javascript">';
            echo 'sweetAlert("Sudoku", "Solved Succesfully", "success")';  
            echo '</script>';
        }else{
            echo '<script language="javascript">';
            echo 'sweetAlert("Sudoku", "Sorry Cannt Solve", "error")';  
            echo '</script>';
        }
    }
}




sudokuSolver::sweet();