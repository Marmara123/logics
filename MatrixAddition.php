<?php

function matrix_addition(array $a, array $b): array {
    $sizeofa= sizeof($a);
    $sizeofb= sizeof($b);
    $final= [];
    for($i=0; $i<$sizeofa; $i++){
      for($k=0; $k<$sizeofb; $k++){
        $final[$i][$k]= $a[$i][$k]+$b[$i][$k];
      }
    }
    return $final;
  }