<?php

function reverseWords($str) {
  $exp= explode(" ", $str);
  $size= sizeof($exp);
  $arr=[];
  for($i=0; $i<$size; $i++){
    $new= strrev($exp[$i]);
    array_push($arr, $new);
  }
  return implode(" ", $arr);
}